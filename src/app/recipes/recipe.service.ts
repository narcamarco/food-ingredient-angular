import { EventEmitter, Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();

  // private recipes: Recipe[] = [
  //   new Recipe(
  //     'A Test Recipe1',
  //     'This is a simply a test2',
  //     'https://www.dangoldinc.com/pub/media/wysiwyg/recipe/recipe-img.jpg',
  //     [new Ingredient('Meat', 2), new Ingredient('French Fries', 20)]
  //   ),
  //   new Recipe(
  //     'A Test Recipe2',
  //     'This is a simply a test2',
  //     'https://www.dangoldinc.com/pub/media/wysiwyg/recipe/recipe-img.jpg',
  //     [new Ingredient('Bun', 5), new Ingredient('Patty', 10)]
  //   ),
  //   new Recipe(
  //     'A Test Recipe3',
  //     'This is a simply a test3',
  //     'https://www.dangoldinc.com/pub/media/wysiwyg/recipe/recipe-img.jpg',
  //     [new Ingredient('Strawberry', 4), new Ingredient('Coconut', 17)]
  //   ),
  // ];

  private recipes: Recipe[] = [];
  
  constructor(private slService: ShoppingListService) {}

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;

    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }
}
